#!/bin/bash
NAME="autostepca"
TAG=${1:-latest}
if [ "${REGISTRY}" = "" ]; then
    echo "REGISTRY environemnt variable must be set!"
    exit 1
fi
FULL="${REGISTRY}${NAME}:${TAG}"
docker tag "${NAME}" "${FULL}"
docker push "${FULL}"
