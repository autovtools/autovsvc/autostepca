#!/bin/bash

function show_usage(){
    echo "Usage: ${0} CA_URL"
    echo "  CA_URL: Location of the step-ca server."
    echo "  Example: https://ca.localdomain:9000"
    exit 1
}
if [ "$1" = "" ] || [ "$1" = "-h" ] ; then
    show_usage
fi
CA_FQDN="$(echo "${1}" | cut -d '/' -f3 | cut -d ':' -f1)"
CA_URL="${1}/roots"

curl -k -s "${CA_URL}" | jq -r '.crts  | join("\n")' > "${CA_FQDN}.crt"

DEST=""
if [ -x "$(command -v update-ca-certificates)" ]; then
    DEST="/usr/local/share/ca-certificates/${CA_FQDN}.crt"
    sudo mv "${CA_FQDN}.crt" "${DEST}" || exit 1
    sudo update-ca-certificates || exit 1
else
    # Probably RedHat
    DEST="/etc/pki/ca-trust/source/anchors/${CA_FQDN}.crt"
    sudo mv "${CA_FQDN}.crt" "${DEST}" || exit 1
    sudo update-ca-trust || exit 1
fi
printf "Cert from:\n${CA_URL}\ninstalled at:\n${DEST}\n"
