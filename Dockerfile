FROM smallstep/step-ca:latest
# Use jq to programatically insert 'claims' block into ca.json
# https://github.com/smallstep/cli/issues/125
# https://github.com/katoni/simple-acme-server/blob/master/
USER root
RUN apk --update --no-cache add jq ;\
    mkdir -p /export ;\
    chown step:step /export

COPY ./entrypoint.sh /entrypoint.sh

USER step
ENTRYPOINT ["/entrypoint.sh"]
