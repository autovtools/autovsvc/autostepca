#!/bin/sh

##### From environment #####
CA_URL="${CA_URL:-https://ca.localdomain:9000}"

# The largest unit available is hours, see --not-after:
#   https://smallstep.com/docs/cli/ca/certificate/
MAX_DURATION="${MAX_DURATION:-720h}"
DEFAULT_DURATION="${DEFAULT_DURATION:-${MAX_DURATION}}"

##### Helper funcs #####
function randpass(){
    LEN=${1:-64}
    base64 </dev/urandom | head -c "${LEN}"
}

##### Constants, etc. #####
CA_JSON="/home/step/config/ca.json"
CA_CERT="/home/step/certs/root_ca.crt"
SECRETS="/home/step/secrets"
PKI_PASS="${SECRETS}/password"

PROVISIONER="autocertman"
PROVISIONER_PASS="/export/${PROVISIONER}.pass"
CA_FINGERPRINT="/export/ca.fingerprint"
CA_URL_EXPORT="/export/ca.url"
CA_CERT_EXPORT="/export/root_ca.crt"

# Extract tokens from CA_URL
#   (Busybox does not have --complement)
CA_FQDN="$(echo $CA_URL | cut -d '/' -f3 | cut -d ':' -f1)"
CA_NAME="${CA_FQDN} CA"
CA_ADDR=":$(echo $CA_URL | cut -d '/' -f3 | cut -d ':' -f2)"
CA_HOSTNAME="$(echo $CA_FQDN | cut -d '.' -f 1)"
DOMAIN=$(echo "${CA_FQDN}" | sed "s/${CA_HOSTNAME}\.//")

##### SCRIPT START #####

if [ -f ${CA_JSON} ]; then
    echo "Using config at ${CA_JSON}"
else
    # Generate good passwords
    mkdir -p "${SECRETS}"
    randpass > "${PKI_PASS}"
    randpass > "${PROVISIONER_PASS}"

    step ca init --name "${CA_NAME}" --provisioner "${PROVISIONER}" --dns "${CA_FQDN}" --address "${CA_ADDR}" --password-file "${PKI_PASS}" --provisioner-password-file "${PROVISIONER_PASS}"
    # Increase certificate validity period
    #   https://github.com/smallstep/cli/issues/125
    #   https://github.com/smallstep/certificates/blob/master/docs/GETTING_STARTED.md#whats-inside-cajson
    JQ='.authority.provisioners[[.authority.provisioners[] | .name=="'${PROVISIONER}'"] | index(true)].claims |= (. + {"maxTLSCertDuration":"'${MAX_DURATION}'","defaultTLSCertDuration":"'${DEFAULT_DURATION}'"})'

    echo $(cat "${CA_JSON}" | jq "${JQ}" ) > "${CA_JSON}"
fi

# Export remaining data
step certificate fingerprint "${CA_CERT}" > "${CA_FINGERPRINT}"
echo "${CA_URL}" > "${CA_URL_EXPORT}"
cp "${CA_CERT}" "${CA_CERT_EXPORT}"
chmod a+r "${CA_CERT_EXPORT}"

# Start the step-ca server
STEP_CA="$(which step-ca)"
exec "${STEP_CA}" -password-file "${PKI_PASS}" "${CA_JSON}"
